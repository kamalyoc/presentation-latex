const int trigger = 5; //Trigger pin 
const int echo = 3; //Echo pin 
long temps;
int dist;
unsigned long lastProjectileTime;

void setup() {

  Serial.begin(115200); 
  Serial.setTimeout(1);
  pinMode(trigger, OUTPUT); 
  pinMode(echo, INPUT); 
}

/*###Function to calculate distance###*/
void calculate_distance(int trigger, int echo){
  digitalWrite(trigger, LOW);
  delayMicroseconds(2);
  digitalWrite(trigger, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigger, LOW);

  temps = pulseIn(echo, HIGH);
  dist= temps*0.034/2;
}

// the loop function runs over and over again forever
void loop() {
  unsigned long currentTime = millis();
  if ( (currentTime - lastProjectileTime)< 1000){
    return;}
  calculate_distance(trigger,echo);
  //Serial.println(dist); 
  if (dist<20){
    lastProjectileTime = currentTime;
    Serial.println("Attack"); 
    delay(5);
  //}else{
    //Serial.println("Stop"); 
    //delay(5);   
  }             
}
