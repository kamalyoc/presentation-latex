import pygame
import random


#classe pour les comet
class Comet(pygame.sprite.Sprite):
    
    def __init__(self, comet_event):
        super().__init__()
        # definir l'image du comet
        self.image = pygame.image.load('assets/comet.png')
        #self.image = pygame.transform.scale(self.image, (50,50))
        self.rect = self.image.get_rect()
        self.rect.x = random.randint(20, 1200)
        self.rect.y = - random.randint(0, 800)
        self.velocity = random.randint(1, 3)
        self.comet_event = comet_event
        
    def remove(self):
        self.comet_event.all_comets.remove(self)
        
        # le son
        self.comet_event.game.sound_manager.play('meteorite')
        
        # verifier si le nombre de comettes est de 0
        if len(self.comet_event.all_comets) == 0:
            #remettre la barre à 0
            self.comet_event.reset_percent()
            # apparître les 2 premiers monstres
            self.comet_event.game.start()
    
    
    def fall(self):
        self.rect.y += self.velocity
        
        # ne tombe pas sur le sol
        if self.rect.y>= 1000:
            self.remove()
        
        # si il n'y a plus de boules de comets
        if len(self.comet_event.all_comets) == 0:
            #remettre la jauge au départ
            self.comet_event.reset_percent()
            self.comet_event.fall_mode = False
        
        # verifier si la boule de feu touche le joueur
        if self.comet_event.game.check_collision(
            self, self.comet_event.game.all_players
        ):
            ##print("7aseb ala !!")
            self.remove()
            #subir 20 points de dégats
            self.comet_event.game.player.damage(20)

