import pygame
from comet import Comet


#Classe pour gérer l'evenement du comet
class CometFallEvent:
    
    def __init__(self, game):
        self.percent = 0
        self.percent_speed = 10
        self.game = game
        self.fall_mode = False
        
        #definir un group de sprite pour les comets
        self.all_comets = pygame.sprite.Group()
    
    def add_percent(self):
        self.percent += self.percent_speed/100
    
    def is_full_loaded(self):
        return self.percent >= 100
    
    def reset_percent(self):
        self.percent = 0
     
    def meteor_fall(self):
        #boucle pour valeurs entre 1 et 10
        for i in range(1,10):       
            #apparaitre 1 boule de feu
            self.all_comets.add(Comet(self))
     
    def attempt_fall(self):
        if self.is_full_loaded() and len(self.game.all_monsters) == 0:
            ##print("A7ooooooooooooooo nayazek ala")
            self.meteor_fall()
            self.fall_mode = True # activer l'evenement
            
    
    def update_bar(self, surface):
        
        #ajouter du pourcentage à la bar
        self.add_percent()
        
        # barre noir ( en arriere plan )
        pygame.draw.rect(surface, (0, 0, 0), [
            0, # l'axe des x
            surface.get_height()-20, #l'axe des y
            surface.get_width(), # longueur de l fenetre
            10 # epaisseur
            ])
        # barre bleu de l'evenemt
        pygame.draw.rect(surface, (36, 62, 234), [
            0, # l'axe des x
            surface.get_height()-20, #l'axe des y
            (surface.get_width() / 100) *self.percent, # longueur de l fenetre
            10 # epaisseur
            ])
        