import pygame
from player import Player
from monster import Enemie
from monster import Alien
from comet_event import CometFallEvent
from sound import SoundManager

#Classe du jeu
class Game:
    
    def __init__(self):
        #if le jeu a commencé ou non
        self.is_playing = False
        #generer notre joueur
        self.all_players = pygame.sprite.Group()
        self.player  = Player(self)
        self.all_players.add(self.player)
        # generer l'evenement
        self.comet_event = CometFallEvent(self)
        #group de monstre
        self.all_monsters = pygame.sprite.Group()
        #gerer le son
        self.sound_manager = SoundManager()
        # mettre le score à zéro
        self.score = 0
        self.pressed = {}
        self.bg=0
        
                
    
    def start(self):
        self.is_playing = True
        self.spawn_monster(Enemie)
        self.spawn_monster(Enemie)
        self.spawn_monster(Enemie)
        self.spawn_monster(Enemie)
        self.spawn_monster(Alien)
        self.spawn_monster(Alien)
    
    def game_over(self):
        #refaire le jeu par retirer les monstres, remettre la vie à 100, arrêter de jouer
        self.bg=0
        self.all_monsters = pygame.sprite.Group() #refaire le group de monstres pour ne pas surcharger
        self.comet_event.all_comets = pygame.sprite.Group()
        self.player.health = self.player.max_health
        self.comet_event.reset_percent()
        self.is_playing = False
        self.score = 0  
        #son
        self.sound_manager.play('game_over')
        
    
    def update (self, screen):
        # afficher le score sur l'écran
        font = pygame.font.Font("assets/google-font.ttf",25)
        score_text = font.render(f"Score : {self.score}", 1, (0,0,0))
        screen.blit(score_text, (20, 20))
        
        #appliquer l'image de mon joueur
        screen.blit(self.player.image, self.player.rect)
        #barre de vie du joueur
        self.player.update_health_bar(screen)
    
        #recupere les projectiles du joueur
        for projectile in self.player.all_projectiles:
            projectile.move()
            
        #actualiser la barre d'evenement du jeu
        self.comet_event.update_bar(screen)
        
        #actualiser l'animation du joueur
        self.player.update_animation()
        
        #recuperer les monstres du jeu
        for monster in self.all_monsters:
            monster.forward()
            monster.update_health_bar(screen)
            monster.update_animation()
            
        #recuperer les comets de notre jeu
        for comet in self.comet_event.all_comets:
            comet.fall()
         
        #appliquer l'ensemble des images de mon gp projectiles
        self.player.all_projectiles.draw(screen)
    
        #appliquer l'ensemble des images de mon gp de monstre
        self.all_monsters.draw(screen)
        
        # appliquer l'ensemble des images de mon gp comet
        self.comet_event.all_comets.draw(screen)
    
        #verifier la direction
        if self.pressed.get(pygame.K_RIGHT) and ( self.player.rect.x + self.player.rect.width <screen.get_width() ): 
           self.player.move_right()
        elif self.pressed.get(pygame.K_LEFT) and self.player.rect.x >0:
          self.player.move_left()           
        elif self.pressed.get(pygame.K_DOWN) and ( self.player.rect.y < (screen.get_height()-200) ):
            self.player.move_down()
        elif self.pressed.get(pygame.K_UP) and ( self.player.rect.y > 0 ):
            self.player.move_up()
    
    
    def check_collision(self, sprite, group):
        return pygame.sprite.spritecollide(sprite, group, False, pygame.sprite.collide_mask)
    
    def spawn_monster(self, monster_class_name):
        self.all_monsters.add(monster_class_name.__call__(self))
        
        
        
        
        
        