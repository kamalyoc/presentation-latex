import math
import pygame
import serial
import time
##################################la main fonction#################################

from game import Game
pygame.init()

# Crée la relation entre le capteur distance et python
ArduinoSerial = serial.Serial('COM5',115200,timeout=.1) 


# Definir horloge
#clock = pygame.time.Clock()
#FPS = 120


#genere la fenetre du jeu
pygame.display.set_caption("Comet fall game")
screen = pygame.display.set_mode((1400 , 960))

#les backgrounds du jeu
background = pygame.image.load('assets/Background.jpg')
background = pygame.transform.scale(background, (1400,960))

Egypte = pygame.image.load('assets/Egypte.jpg')
Egypte = pygame.transform.scale(Egypte, (1400,960))

Space = pygame.image.load('assets/Space.png')
Space = pygame.transform.scale(Space, (1400,960))

#Pour choisir le background
SmallEgypte = pygame.transform.scale(Egypte, (300,200))
SmallEgypte_rect = SmallEgypte.get_rect()
SmallEgypte_rect.x = math.ceil(screen.get_width() / 6)
SmallEgypte_rect.y = math.ceil(screen.get_height() / 1.5)

SmallSpace = pygame.transform.scale(Space, (300,200))
SmallSpace_rect = SmallEgypte.get_rect()
SmallSpace_rect.x = math.ceil(screen.get_width() / 1.9)
SmallSpace_rect.y = math.ceil(screen.get_height() / 1.5)


#bouton de lancement du jeu
play_button = pygame.image.load('assets/button.png')
play_button = pygame.transform.scale(play_button, (400,150))
play_button_rect = play_button.get_rect()
play_button_rect.x = math.ceil(screen.get_width() / 3)
play_button_rect.y = math.ceil(screen.get_height() / 2)

# charger notre jeu
game = Game()

BoucleInfini = True

# lire les sortie du capteur distance
def func():
    incoming = str (ArduinoSerial.readline()) #read the serial data and print it as line
    if 'Attack' in incoming:
        game.player.launch_projectile()





# boucle while cette condition est True
while BoucleInfini:
    
    
    #appliquer l'arriere plan de notre jeu
    if game.bg==0:
        screen.blit(background, (0,0))
    elif game.bg==1 :
        screen.blit(Space, (0,0))
    else:
        screen.blit(Egypte, (0,0))
    
    #Verifier if le jeu a commencé ou non
    if game.is_playing:
        #déclencher les instruction de la partie
        game.update(screen)
    else:
        # ajouter l'écran d'accueil
        screen.blit(play_button, play_button_rect)
        screen.blit(SmallEgypte, SmallEgypte_rect)
        screen.blit(SmallSpace, SmallSpace_rect)
    
    #mettre à jour l'écran
    pygame.display.flip()
    
    #détecter un evenement     

# la sortie du capteur de distance
    if ArduinoSerial.in_waiting > 0:
        incoming = str (ArduinoSerial.readline()) #read the serial data and print it as line
        if 'Attack' in incoming:
           game.player.launch_projectile()
            
       
    for event in pygame.event.get():
        #fermeture de fenetre
        if event.type == pygame.QUIT:
            BoucleInfini = False
            pygame.quit()
            print("Fermeture du jeux")
        #touche de clavier
        elif event.type == pygame.KEYDOWN:
            game.pressed[event.key] = True
            
            #detecter espace pour lancer projectiles
            if (event.key == pygame.K_SPACE):
                game.player.launch_projectile()
             
            
        elif event.type == pygame.KEYUP:
            game.pressed[event.key] = False
            
        elif event.type == pygame.MOUSEBUTTONDOWN:
            #si la souris appui sur le bouton
            if SmallSpace_rect.collidepoint(event.pos):
                game.bg = 1
            elif SmallEgypte_rect.collidepoint(event.pos):
                game.bg = 2
                
            if play_button_rect.collidepoint(event.pos):
                #lancer le jeu
                game.start()
                # joueu le son
                game.sound_manager.play('click')
                
                
          
            
    #fixer le nombre de fps pour la clock
#    clock.tick(FPS)        
            
            
