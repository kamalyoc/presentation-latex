import pygame
import random
import animation


#les monstres enemies 
class Monster(animation.AnimateSprite):
    
    def __init__(self, game, name, size, offset=0):
        super().__init__(name, size)
        self.game = game
        
        self.health     = 100 #point de vie qui va varier
        self.max_health = 100 #point de vie initial
        self.attack     = 0.3   #attack damage
        
        #self.image    = pygame.image.load('assets/mummy.png')
        self.rect = self.image.get_rect()
        
        self.rect.x = random.randint(1400, 1800)
        self.rect.y = random.randint(10, 800)
        self.start_animation()
        
        
    def set_speed(self, speed):
        self.default_speed = speed
        self.velocity   = random.randint(1, 3)   #movement speed
        
    
    def damage(self, amount):
        self.health -= amount
        
        #si il est mort
        if self.health <= 0:
            #Reapparaitre comme un nouveau monstre
            self.rect.x = random.randint(1400, 1800)
            self.rect.y = random.randint(10, 800)
            self.velocity   = random.randint(1, self.default_speed)   #movement speed
            self.health = self.max_health
            # ajouter le nombre de points
            self.game.score += 20
            
            # si la barre d'evenement est chargé à son maximum
            if self.game.comet_event.is_full_loaded():
                # retirer du jeu
                self.game.all_monsters.remove(self)
                
                #déclencher la plui
                self.game.comet_event.attempt_fall()
    
    
    def update_animation(self):
        self.animate(loop=True)
    
    def update_health_bar(self, surface):
        #la couleur de la vie
        bar_color = (227, 51, 40)#rouge
        #la couleur de l'arriere de la jauge
        back_bar_color = (60,63,60)#gris
        
        #position de la jauge de ie et les dimensions
        bar_position = [self.rect.x+60, self.rect.y+50, self.health, 5]
        #l'arriere de la jauge
        back_bar_position = [self.rect.x+60, self.rect.y+50, self.max_health, 5]
        
        #dessiner la barre
        pygame.draw.rect(surface, back_bar_color, back_bar_position)
        pygame.draw.rect(surface, bar_color, bar_position)
        
    
    def forward(self):
        #si il est arrivé à la fin
        if self.rect.x <=0 :
            self.damage(self.max_health)
        else:
            #check les collision
            if not self.game.check_collision(self, self.game.all_players):
                # si il n'est pas en collision il bouge
                self.rect.x -= self.velocity
            else:
                # si il est il attack
                self.game.player.damage(self.attack)
            

# definir une classe pour la momie
class Enemie(Monster):
    
    def __init__(self, game):
        super().__init__(game, "enemie", (300,300))
        self.set_speed(3)
        
        
            
# definir une classe pour l'alien
class Alien(Monster):
    
    def __init__(self, game):
        super().__init__(game, "alien", (500,500), 130)
        self.health     = 250 #point de vie qui va varier
        self.max_health = 250 #point de vie initial
        self.set_speed(1)
        self.attack = 0.8
            
            
            
            
            
            
            
            
            