import pygame
from projectile import Projectile
import animation
import time


#Classe pour représenter le joueur
class Player(animation.AnimateSprite):
    
    def __init__(self, game):
        super().__init__('player')
        self.game = game
        
        self.health     = 100 #point de vie qui va varier
        self.max_health = 100 #point de vie initial
        self.attack     = 10  #attack damage
        self.velocity   = 2   #movement speed
        self.all_projectiles = pygame.sprite.Group()
        
        #self.image = pygame.image.load('assets/player.png')
        self.rect = self.image.get_rect()
        self.rect.x = 80
        self.rect.y = 400
        
    
    def damage(self, amount):
        if (self.health - amount) > amount :
            self.health -= amount
        else:
            #si il est mort
            self.game.game_over()
    
    def update_animation(self):
        self.animate()
        
    def update_health_bar(self, surface):
        #la couleur de la vie
        bar_color = (111, 210, 46)#vert
        #la couleur de l'arriere de la jauge
        back_bar_color = (60,63,60)#gris
        
        #position de la jauge de ie et les dimensions
        bar_position = [self.rect.x, self.rect.y+20, self.health, 7]
        #l'arriere de la jauge
        back_bar_position = [self.rect.x, self.rect.y+20, self.max_health, 7]
        
        #dessiner la barre
        pygame.draw.rect(surface, back_bar_color, back_bar_position)
        pygame.draw.rect(surface, bar_color, bar_position)
    
    def launch_projectile(self):
        # creer une nouvelle instance de la classe Projectile
        self.all_projectiles.add(Projectile(self))
        # demarrer l'animation 
        self.start_animation()
        # le son
        self.game.sound_manager.play('tir')
        
        
        
    def move_right(self):
        #check les collisions
        if not self.game.check_collision(self, self.game.all_monsters):
            self.rect.x += self.velocity
        
    def move_left(self):
        self.rect.x -= self.velocity
        
    def move_up(self):
        #check les collisions
#        if not self.game.check_collision(self, self.game.all_monsters):
        self.rect.y -= self.velocity   
        
    def move_down(self):
        #check les collisions
#        if not self.game.check_collision(self, self.game.all_monsters):
        self.rect.y += self.velocity
        
        
        
        