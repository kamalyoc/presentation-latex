LaTeX : est un diaporama de présentation pour le UE bureau d'études

Python : un jeu utilisant pygame
        utilisant un capteur de distance connecté à Stm32 Nucleo F446RE qui envoie la valeure de la distance au code python grace à la libraire pyserial

*Important*    Pour tester le jeu sans le capteur de distance c'est suffisant de supprimer les lignes : 11, 58-61, 95-98 du script main.py

Pour commencer le jeu on choisi le terrain de jeu, ensuite on appui play
nous pouvons bouger dans les 4 directions, et attaqué avec la touché espace (ou s'approcher du capteur de distance)
nous devons éviter les monstres ou les tuer
après un certain temps, les monstres s'arrêtent de sortir et des commets tombent que nous sommes obligé de les évader
